import chalk from 'chalk'
import { votingLogDetails } from '../db/models'

const ERROR = chalk.bold.red

const getVotingLog = async (req, res) => {
	try {
		const votingLogs = await votingLogDetails.findAll()
		res.send(votingLogs)
	} catch (e) {
		res.status(400).send(ERROR('error is', e))
	}
}

export default getVotingLog
