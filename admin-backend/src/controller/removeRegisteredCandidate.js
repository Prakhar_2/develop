/* eslint-disable no-restricted-syntax */
import removeCandidate from '../services/dbServices/removeCandidate'

const removeRegisteredCandidate = async (req, res) => {
	try {
		const { candidateAddress } = req.body
		const result = await removeCandidate(candidateAddress)
		res.send(result)
	} catch (e) {
		res.status(400).send('ERROR')
	}
}

export default removeRegisteredCandidate
