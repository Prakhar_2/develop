module.exports = (sequelize, DataTypes) => {
	const RegisteredCandidateDetails = sequelize.define(
		'registeredCandidateDetails',
		{
			candidate_Address: {
				type: DataTypes.STRING,
				allowNull: false,
				primaryKey: true,
			},

			name: {
				type: DataTypes.STRING,
				allowNull: false,
			},
		}
	)
	return RegisteredCandidateDetails
}
