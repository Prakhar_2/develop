module.exports = (sequelize, DataTypes) => {
	const VotingLogDetails = sequelize.define('votingLogDetails', {
		candidate_Address: {
			type: DataTypes.STRING,
			allowNull: false,
		},

		voter_Address: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	})
	return VotingLogDetails
}
