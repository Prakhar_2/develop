import web3 from './web3'
import abi from '../common/abi/abis.json'
import config from '../config/app'

const contractInstance = new web3.eth.Contract(abi, config.contractAddress)

export default contractInstance
