import Web3 from 'web3'
import config from '../config/app'

export const web3Plain = new Web3(
	new Web3.providers.HttpProvider(config.web3Url)
)

const web3 = new Web3(
	new Web3.providers.WebsocketProvider('ws://localhost:7545')
)
export default web3
