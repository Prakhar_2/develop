/* eslint-disable no-console */
/* eslint-disable func-names */
/* eslint-disable no-underscore-dangle */
// import chalk from 'chalk'
import web3 from '../lib/web3'
import config from '../config/app'
import contractInstance from '../lib/contract'
import addRegisteredCandidate from '../services/dbServices/addRegisteredCandidate'

const RegisteredCandidateEvent = async (socket) => {
	const eventDetails = contractInstance._jsonInterface.find(
		({ name }) => name === 'registeredCandidateEvent'
	)
	const { inputs, signature } = eventDetails

	web3.eth.subscribe(
		'logs',
		{
			address: config.contractAddress,
			topics: [signature],
		},
		async function (error, result) {
			if (!error) {
				const eventObj = web3.eth.abi.decodeLog(
					inputs,
					result.data,
					result.topics.slice(1)
				)

				await addRegisteredCandidate(eventObj.name, eventObj.candidateAddress)
				socket.emit('registeredCandidate', {
					name: eventObj.name,
					candidateAddress: eventObj.candidateAddress,
				})
			} else {
				console.log('default:', error)
			}
		}
	)
}

export default RegisteredCandidateEvent
