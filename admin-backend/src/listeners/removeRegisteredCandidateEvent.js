/* eslint-disable no-console */
/* eslint-disable func-names */
/* eslint-disable no-underscore-dangle */
import web3 from '../lib/web3'
import config from '../config/app'
import contractInstance from '../lib/contract'

const removeRegisteredCandidateEvent = async (socket) => {
	const eventDetails = contractInstance._jsonInterface.find(
		({ name }) => name === 'getRegisteredCandidateAfterRemoveEvent'
	)

	const { inputs, signature } = eventDetails

	web3.eth.subscribe(
		'logs',
		{
			address: config.contractAddress,
			topics: [signature],
		},
		async function (error, result) {
			if (!error) {
				const eventObj = web3.eth.abi.decodeLog(
					inputs,
					result.data,
					result.topics.slice(1)
				)
				const deletedCandidate = {
					name: eventObj.name,
					candidateAddress: eventObj.candidateAddress,
				}
				socket.emit('removedCandidate', deletedCandidate)
			} else {
				console.log('default:', error)
			}
		}
	)
}

export default removeRegisteredCandidateEvent
