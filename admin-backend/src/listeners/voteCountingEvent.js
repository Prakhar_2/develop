/* eslint-disable no-console */
/* eslint-disable func-names */
/* eslint-disable no-underscore-dangle */
import chalk from 'chalk'
import web3 from '../lib/web3'
import config from '../config/app'
import contractInstance from '../lib/contract'
import addVotingLog from '../services/dbServices/addVotingLog'

const success = chalk.bold.yellowBright

const VoteCountingEvent = async (socket) => {
	const eventDetails = contractInstance._jsonInterface.find(
		({ name }) => name === 'VoteCountingEvent'
	)
	const { inputs, signature } = eventDetails

	web3.eth.subscribe(
		'logs',
		{
			address: config.contractAddress,
			topics: [signature],
		},
		async function (error, result) {
			if (!error) {
				const eventObj = web3.eth.abi.decodeLog(
					inputs,
					result.data,
					result.topics.slice(1)
				)
				const response = await addVotingLog(
					eventObj.voterAddress,
					eventObj.candidateAddress
				) // database entry
				console.log(chalk.bold.red('VoterAddress is ', eventObj.voterAddress))
				console.log(
					chalk.bold.greenBright(
						'candidateAddress is ',
						eventObj.candidateAddress
					)
				)

				console.log('votingLogEvent response is ', eventObj)

				if (response) {
					const LogDetails = {
						voterAddress: eventObj.voterAddress,
						candidateAddress: eventObj.candidateAddress,
					}
					// Emitting a new message. Will be consumed by the client
					socket.emit('votingLog', LogDetails)
				}
				console.log(chalk.bold.redBright('databaseResponse', success(response)))
			} else console.log('default:', error)
		}
	)
}

export default VoteCountingEvent
