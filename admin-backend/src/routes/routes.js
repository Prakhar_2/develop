import express from 'express'
import votingLog from '../controller/votingLog'
import previousVotingLog from '../controller/previousVotingLog'
import registeredCandidates from '../controller/getRegisteredCandidate'
import removeRegisteredCandidate from '../controller/removeRegisteredCandidate'

const Router = express.Router()

Router.get('/data', votingLog)
Router.get('/previousVotingLog', previousVotingLog)
Router.get('/getRegisteredCandidates', registeredCandidates)
Router.post('/removeRegisteredCandidate', removeRegisteredCandidate)

export default Router
