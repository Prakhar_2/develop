import { registeredCandidateDetails } from '../../db/models'

const addRegisteredCandidate = async (name, candidateAddress) => {
	try {
		await registeredCandidateDetails.create({
			candidate_Address: candidateAddress,
			name,
		})
		return 1
	} catch {
		return 0
	}
}
export default addRegisteredCandidate
