import { votingLogDetails } from '../../db/models'

const addVotingLog = async (voterAddress, candidateAddress) => {
	try {
		await votingLogDetails.create({
			candidate_Address: candidateAddress,
			voter_Address: voterAddress,
		})
		return 1
	} catch {
		return 0
	}
}
export default addVotingLog
