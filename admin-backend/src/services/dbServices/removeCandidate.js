import { registeredCandidateDetails } from '../../db/models'

const removeCandidate = async (candidateAddress) => {
	await registeredCandidateDetails.destroy({
		where: {
			candidate_Address: candidateAddress,
		},
	})
	const result = await registeredCandidateDetails.findAll()
	return result
}

export default removeCandidate
