import { contractInstance } from '../index'

export const getOwnerAddress = async () => {
	try {
		const res = await contractInstance.methods.owner.call().call()
		return res
	} catch (e) {
		console.log('error is ', e)
	}
}
