/* eslint-disable no-console */
import contractInstance from '../../lib/contract'

// eslint-disable-next-line consistent-return
const getVotingStatus = async () => {
	try {
		const res = await contractInstance.methods.getVotingStatus().call()
		return res
	} catch (e) {
		console.log('ERROR IS', e)
	}
}

export default getVotingStatus
