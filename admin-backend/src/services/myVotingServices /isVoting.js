const contractInstance = require('../const/contractDetail')

const isVoting = async () => {
	const result = await contractInstance.methods.isVoting.call().call()
	return result
}
module.exports= isVoting
