/* eslint-disable no-console */
import contractInstance from '../../lib/contract'

const voteForCandidate = async (candidateAddress, voterAddress) => {
	try {
		await contractInstance.methods
			.voteForCandidate(candidateAddress)
			.send({ from: voterAddress })
	} catch (e) {
		console.log('Error getting', e)
	}
}

export default voteForCandidate
