import './App.css'
import Admin from './component/admin/admin'

function App() {
	return (
		<div className='App'>
			<Admin />
		</div>
	)
}

export default App
