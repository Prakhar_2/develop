import React from 'react'
import { useSelector } from 'react-redux'
import './Indii.css'

const Indi = () => {
	let votingStatus = useSelector((state) => state.votingLog.value)

	return (
		<div class='container'>
			{votingStatus === true ? (
				<div class='led-box' style={{ marginLeft: '2Vw' }}>
					<div class='led-green'></div>
				</div>
			) : (
				<div class='led-box' style={{ marginLeft: '2Vw' }}>
					<div class='led-red'></div>
				</div>
			)}
		</div>
	)
}

export default Indi
