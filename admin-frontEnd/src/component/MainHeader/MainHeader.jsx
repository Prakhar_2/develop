import React from 'react'
import Indicator from './Indicator/Indicator'
import { Typography } from '@material-ui/core'

const MainHeader = () => {
	return (
		<div>
			<div style={{ display: 'flex', backgroundColor: 'azure' }}>
				<div style={{ display: 'flex', marginLeft: 'auto', marginRight: '2%' }}>
					<Typography
						style={{
							marginTop: '1Vh',
							fontSize: '20px',
							fontWeight: '900',
							color: '#3f51b5',
						}}
					>
						voting Status{' '}
					</Typography>
					<Indicator />
				</div>
			</div>
		</div>
	)
}

export default MainHeader
