import React, { useState } from 'react'
import { Button } from '@material-ui/core'
import PropTypes from 'prop-types'
import { Box, Typography } from '@material-ui/core'
import Header from './header/header'
import Winner from '../winner/winner'
import MainHeader from '../MainHeader/MainHeader'
import '../../text.css'
import RegisteredCandidate from '../table/registredCandidate/RegisteredCandidate'
import WhiteListedCandidate from '../table/whitelistedCandidate/WhitelistedCandidate'
import { getAdmin } from '../../myVotingServices/getAdmin'
import VotingStatus from '../table/votingStatus/VotingStatusUpdate'
import { useSelector } from 'react-redux'

const Item = (props) => {
	const { ...other } = props
	return <Box {...other} />
}

Item.propTypes = {
	sx: PropTypes.object,
}

const AdminTask = () => {
	const [isAdmin, setAdminStatus] = useState(false)
	let selectedAddressIs = ''

	if (window.ethereum.selectedAddress !== null) {
		selectedAddressIs = window.ethereum.selectedAddress.toLowerCase()
	}

	const [selectedAddress, setSelectedAddress] = useState(selectedAddressIs)

	window.ethereum.on('accountsChanged', (accounts) => {
		setSelectedAddress(accounts[accounts.length - 1])
	})

	const init = async () => {
		const adminAddress = await getAdmin()

		if (adminAddress && adminAddress.toLowerCase() === selectedAddress)
			setAdminStatus(true)
		else setAdminStatus(false)
	}

	init()

	let votingStatus = useSelector((state) => state.votingLog.value)

	return (
		<div style={{ margin: '1.5Vw' }}>
			<div className='sticky'>
				<MainHeader />
			</div>
			{isAdmin === true ? (
				<>
					{' '}
					<Header />
					<div style={{ width: '99%' }}>
						<Box
							style={{
								display: 'grid',
								gridAutoFlow: 'row',
								gridTemplateColumns: 'repeat(2, 60% 40%)',
								gridTemplateRows: 'repeat(1, 100Vh)',
								backgroundColor: 'black',
								gap: '20px',
							}}
						>
							<Item style={{ gridRow: '1', backgroundColor: 'azure' }}>
								{votingStatus === false ? (
									<>
										<div
											style={{ marginTop: '5Vw', textDecoration: 'underline' }}
										>
											<h2>Voting not started plz! whitelist the candidates </h2>{' '}
										</div>
										<div style={{ marginLeft: '10%' }}>
											<RegisteredCandidate />
										</div>
									</>
								) : (
									<> </>
								)}

								<div
									style={{
										marginLeft: '-1Vw',
										marginTop: '3Vw',
										borderRadius: '10px',
									}}
								>
									<Button
										variant='contained'
										color='secondary'
										style={{ borderRadius: '10px' }}
									>
										WhitelistedCandidate are
									</Button>
								</div>

								<div style={{ marginLeft: '10%', marginTop: '3Vh' }}>
									<WhiteListedCandidate />
								</div>
							</Item>

							<Item style={{ backgroundColor: '#0f0e0e' }}>
								{' '}
								{votingStatus === true ? (
									<>
										<div style={{ width: '80%', marginLeft: '10%' }}>
											<Winner />
										</div>
										<div style={{ marginLeft: '1Vw', marginRight: '2Vw' }}>
											<VotingStatus />
										</div>{' '}
									</>
								) : (
									<div className='fo'>
										<h1 className='a'>wait till voting being starts... </h1>
									</div>
								)}
							</Item>
						</Box>
					</div>
				</>
			) : (
				<div
					style={{
						height: '100Vh',
						width: '97%',
						position: 'fixed',
						backgroundColor: 'azure',
					}}
				>
					<Typography
						style={{
							margin: '50px',
							marginTop: '20rem',
							fontFamily: 'serif',
							fontSize: '40px',
						}}
					>
						YOU ARE NOT AN ADMIN
					</Typography>{' '}
				</div>
			)}
		</div>
	)
}

export default AdminTask
