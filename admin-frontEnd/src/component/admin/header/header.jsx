import React from 'react'
import { AppBar, Toolbar, Typography } from '@material-ui/core'
import { endVoting } from '../../../myVotingServices/endVoting'
import { startVoting } from '../../../myVotingServices/startVoting'
import { isVoting } from '../../../myVotingServices/isVoting'
import './header.css'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import { GiStopSign } from 'react-icons/gi'
import { VscDebugStart } from 'react-icons/vsc'
import headerStyles from './style'

const Header = () => {
	const classes = headerStyles()

	const handleStartVoting = async () => {
		const votingStatus = await isVoting()

		if (votingStatus === false) {
			await startVoting()
		} else {
			alert('Voting already started')
		}
	}

	const handleEndVoting = async () => {
		const votingStatus = await isVoting()

		if (votingStatus === true) {
			await endVoting()
		} else {
			alert('Voting NotStarted/ended')
		}
	}

	const [alignment, setAlignment] = React.useState('left')

	const handleAlignment = (event, newAlignment) => {
		setAlignment(newAlignment)
	}

	return (
		<>
			<AppBar position='sticky' className={classes.root1}>
				<Toolbar className={classes.toolbar}>
					<ToggleButtonGroup
						value={alignment}
						exclusive
						onChange={handleAlignment}
						size='large'
						style={{
							backgroundColor: '#cccccc',
							marginLeft: '40rem',
							borderColor: '#99ffff',
						}}
					>
						<ToggleButton
							value='left'
							size='large'
							onClick={() => {
								handleStartVoting()
							}}
						>
							<Typography
								style={{
									fontFamily: 'cursive',
									color: 'black',
									fontWeight: '900',
								}}
							>
								START VOTING
							</Typography>
							<VscDebugStart style={{ fontSize: '60px', width: '90px' }} />
						</ToggleButton>
						<ToggleButton
							value='center'
							size='large'
							onClick={() => {
								handleEndVoting()
							}}
						>
							<Typography
								style={{
									fontFamily: 'cursive',
									color: 'black',
									fontWeight: '900',
								}}
							>
								STOP VOTING
							</Typography>
							<GiStopSign
								style={{ fontSize: '60px', width: '90px', fontWeight: '2000' }}
							/>
						</ToggleButton>
					</ToggleButtonGroup>
				</Toolbar>
			</AppBar>
		</>
	)
}

export default Header
