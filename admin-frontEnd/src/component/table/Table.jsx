import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import MuiAlert from '@material-ui/lab/Alert'

const Alert = (props) => {
	return <MuiAlert elevation={6} variant='filled' {...props} />
}

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const Tablee = ({
	buttonText,
	buttonText1,
	onButtonClick,
	onButtonClick1,
	whiteListedDetails,
	status,
}) => {
	const useStyles = makeStyles({
		table: {
			minWidth: 500,
			maxWidth: 700,
			marginTop: '5%',
		},
	})

	const classes = useStyles()

	const [open, setOpen] = React.useState(false)
	const [ok, setOk] = React.useState(false)

	const handleClick = async (index) => {
		try {
			await onButtonClick(whiteListedDetails[index].address)
		} catch (e) {
			alert('You can not do any thing', e)
		}
	}

	const handleClose = (event, reason) => {
		if (reason === 'clickaway') {
			return
		}
		setOpen(false)
	}

	const handleClick1 = async (index) => {
		setOpen(true)

		try {
			await onButtonClick1(whiteListedDetails[index].address)
			setOk(true)
		} catch (e) {
			alert('You can not do any thing', e)
		}
	}

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							Name{' '}
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							Address{' '}
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							{status}{' '}
						</StyledTableCell>
					</StyledTableRow>
				</TableHead>
				<TableBody>
					{whiteListedDetails.map((ele, index) => {
						return (
							<StyledTableRow data-index={index}>
								<StyledTableCell key={ele.name}>{ele.name}</StyledTableCell>
								<StyledTableCell key={ele.address} id={index}>
									{ele.address}
								</StyledTableCell>
								<div style={{ display: 'flex' }}>
									{buttonText !== '' && buttonText1 !== undefined ? (
										<>
											<StyledTableCell key={Math.random()}>
												{
													<button
														onClick={() => {
															handleClick(index)
														}}
														className={classes.buttons}
													>
														{buttonText}
													</button>
												}
												{ok === true ? (
													<Snackbar
														open={open}
														autoHideDuration={300}
														onClose={handleClose}
													>
														<Alert onClose={handleClose} severity='success'>
															Remove Candidate Request generated successFully!
														</Alert>
													</Snackbar>
												) : (
													<Snackbar
														open={open}
														autoHideDuration={300}
														onClose={handleClose}
													>
														<Alert onClose={handleClose} severity='error'>
															plzz check selected address / Remove Candidate
															Request generated unsuccessFully!
														</Alert>
													</Snackbar>
												)}
												{
													<button
														className={classes.buttons1}
														onClick={() => {
															handleClick1(index)
														}}
													>
														{' '}
														Remove
													</button>
												}
											</StyledTableCell>{' '}
										</>
									) : buttonText1 === undefined && buttonText !== '' ? (
										<StyledTableCell key={Math.random()}>
											{
												<button
													onClick={() => {
														handleClick(index)
													}}
													className={classes.buttons}
												>
													{buttonText}
												</button>
											}
										</StyledTableCell>
									) : (
										<StyledTableCell
											key={Math.random()}
											style={{
												color: 'white',
												fontSize: '26px',
												textAlign: 'end',
											}}
										>
											{ele.voteCount[index]}
										</StyledTableCell>
									)}
								</div>
							</StyledTableRow>
						)
					})}
				</TableBody>
			</Table>
		</div>
	)
}

export default Tablee
