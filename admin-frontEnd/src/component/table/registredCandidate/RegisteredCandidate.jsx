/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import { useSelector } from 'react-redux'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { removeRegistredCandidate } from '../../../myVotingServices/removeRegisteredCandidate'
import { whitelistCandidate } from '../../../myVotingServices/whiteListCandidate'
import axios from 'axios'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const useStyles = makeStyles({
	table: {
		margin: '0px',
		marginTop: '5%',
		border: '6px solid white',
		textAlign: 'left',
		borderStyle: 'double ',
		width: '80%',
	},
})

const RegisteredCandidate = () => {
	const classes = useStyles()

	let registeredCandidateAddress = []
	let registeredCandidateName = []

	const registeredCandidate = useSelector(
		(state) => state.registeredCandidate.value
	)

	const removedRegisteredCandidates = useSelector(
		(state) => state.removedRegisteredCandidate.value
	)

	const removedRegisteredCandidateDetails = new Map()

	for (let removedCandidate of removedRegisteredCandidates)
		removedRegisteredCandidateDetails.set(removedCandidate[0], 1)

	for (let registeredCandidateDetail of registeredCandidate) {
		if (removedRegisteredCandidateDetails.get(registeredCandidateDetail[0])) {
			continue
		} else {
			registeredCandidateAddress.push(registeredCandidateDetail[0])
			registeredCandidateName.push(registeredCandidateDetail[1])
		}
	}

	const handleRemove = async (index) => {
		await removeRegistredCandidate(registeredCandidateAddress[index])
		await axios.post('/removeRegisteredCandidate', {
			candidateAddress: registeredCandidateAddress[index],
		})
	}

	const handleWhiteList = async (index) => {
		await whitelistCandidate(registeredCandidateAddress[index])
	}

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Name
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Address
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Registered
						</StyledTableCell>
					</StyledTableRow>
				</TableHead>
				<TableBody>
					{/* {console.log('array of name', registeredCandidateName)} */}
					{registeredCandidateName.map((ele, index) => {
						return (
							<StyledTableRow data-index={index}>
								<StyledTableCell key={ele}>{ele}</StyledTableCell>
								<StyledTableCell
									key={registeredCandidateAddress[index]}
									id={registeredCandidateAddress[index]}
								>
									{registeredCandidateAddress[index]}
								</StyledTableCell>

								<div
									style={{
										display: 'flex',
										marginTop: '2Vh',
										textAlign: 'center',
									}}
								>
									<button
										onClick={() => {
											handleRemove(index)
										}}
									>
										remove
									</button>
									<button
										onClick={() => {
											handleWhiteList(index)
										}}
										style={{ marginLeft: '1Vw', marginRight: '1Vw' }}
									>
										whiteList
									</button>
								</div>
							</StyledTableRow>
						)
					})}
				</TableBody>
			</Table>
		</div>
	)
}

export default RegisteredCandidate
