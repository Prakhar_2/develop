import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
	table: {
		margin: '0px',
		marginTop: '4Vh',
		border: '6px solid white',
		textAlign: 'left',
		borderStyle: 'double ',
		width: '100%',
	},
	th: {
		border: '3px solid white',
		textAlign: 'center',
		padding: '15px',
		color: '#ff0000',
		fontWeight: 900,
	},
	td: {
		border: '3px solid white',
		textAlign: 'center',
		padding: '15px',
		color: '#ff0000',
		fontSize: '1rem',
	},
	td1: {
		fontSize: '2rem',
		color: '#ff0000',
		width: '100%',
	},
	buttons: {
		color: 'lightBlue',
		textDecoration: 'bold',
		backgroundColor: 'Green',
		fontSize: '1rem',
		fontWeight: 500,
		width: 'auto',
		marginLeft: '0px',
	},
	buttons1: {
		color: 'lightBlue',
		textDecoration: 'bold',
		backgroundColor: 'Red',
		fontSize: '1rem',
		fontWeight: 500,
		width: 'auto',
		marginLeft: '6px',
	},
	card: {
		width: '10%',
	},
}))
