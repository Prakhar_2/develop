import React from 'react'
import { useSelector } from 'react-redux'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import useStyles1 from './style'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const WhiteListedCandidate = () => {
	const classes = useStyles1()

	const whiteListedCandidates = useSelector(
		(state) => state.whiteListedCandidate.value
	)
	const uniqueWhiteListedCandidateAddress = new Set()
	const uniqueWhiteListedCandidateName = new Set()

	let whiteListedCandidateAddress = []
	let whiteListedCandidateName = []

	for (let whiteListedCandidateDetail of whiteListedCandidates) {
		if (whiteListedCandidateDetail.length > 0) {
			uniqueWhiteListedCandidateAddress.add(whiteListedCandidateDetail[0])
			uniqueWhiteListedCandidateName.add(whiteListedCandidateDetail[1])
		}
	}

	for (let item of uniqueWhiteListedCandidateAddress)
		whiteListedCandidateAddress.push(item)
	for (let item of uniqueWhiteListedCandidateName)
		whiteListedCandidateName.push(item)

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							Name{' '}
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							Address{' '}
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							{' '}
							WhiteListed{' '}
						</StyledTableCell>
					</StyledTableRow>
				</TableHead>
				<TableBody>
					{whiteListedCandidateName.map((ele, index) => {
						return (
							<StyledTableRow data-index={index}>
								<StyledTableCell key={ele}>{ele}</StyledTableCell>
								<StyledTableCell
									key={whiteListedCandidateAddress[index]}
									id={whiteListedCandidateAddress[index]}
								>
									{whiteListedCandidateAddress[index]}
								</StyledTableCell>

								<div
									style={{
										display: 'flex',
										marginTop: '2Vh',
										textAlign: 'center',
									}}
								>
									<button style={{ margin: '2Vh' }} disabled>
										{' '}
										whiteListed
									</button>
								</div>
							</StyledTableRow>
						)
					})}
				</TableBody>
			</Table>
		</div>
	)
}

export default WhiteListedCandidate
