import { makeStyles } from '@material-ui/core/styles'

const useStyles1 = makeStyles((theme) => ({
	table: {
		margin: '0px',
		marginTop: '4Vh',
		border: '6px solid white',
		textAlign: 'left',
		borderStyle: 'double',
		width: '80%',
	},
}))

export default useStyles1
