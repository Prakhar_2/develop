import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import store from './redux/store/store'
import Web3 from 'web3'
import axios from 'axios'
import { BrowserRouter } from 'react-router-dom'
import { io } from 'socket.io-client'
import { setVotingStatus } from '../src/redux/reducer/VotingStatus'
import { setVotingLog } from './redux/reducer/votingLog'
import { setRegisteredCandidate } from './redux/reducer/RegisteredCandidate'
import { setRemovedRegisteredCandidate } from './redux/reducer/removedRegisteredCandidate'
import { setWhiteListedCandidate } from './redux/reducer/WhiteListedCandidate'
import { contract } from './const/contractDetail'
import { VoteCountingEvent } from './myVotingServices/voteCountingEvent'
import { getVotingStatus } from './myVotingServices/getVotingStatus'
import { whitelistCandidateEvent } from './myVotingServices/whiteListedCandidateEvent'
import { getWhiteListed } from './myVotingServices/getWhiteListed'
import { votingStatusEvent } from './myVotingServices/votingStatusEvent'
import { isVoting } from './myVotingServices/isVoting'

const web3 = new Web3(window.web3.currentProvider)
// const web3 = new Web3(
// 	new Web3.providers.WebsocketProvider('ws://localhost:7545')
// )
window.ethereum.enable()
export const contractInstance = new web3.eth.Contract(
	contract.ABI,
	contract.address
)

const socket = io('http://localhost:8000')

socket.on('registeredCandidate', (data) => {
	let registeredCandidateDetails = [data.candidateAddress, data.name]
	store.dispatch(setRegisteredCandidate(registeredCandidateDetails))
})

socket.on('removedCandidate', (data) => {
	let deletedRegisteredCandidateDetails = [data.candidateAddress, data.name]
	store.dispatch(
		setRemovedRegisteredCandidate(deletedRegisteredCandidateDetails)
	)
})

VoteCountingEvent()
whitelistCandidateEvent()
votingStatusEvent()

const init = async () => {
	const votingLogs = await getVotingStatus()
	const votingStatus = await isVoting()
	const whiteListedCandidates = await getWhiteListed()
	const registeredCandidates = await axios.get('/getRegisteredCandidates')

	let votingDetails = []

	for (let i = 0; i < votingLogs.voterList1.length; i++) {
		let votingDetailLog = {
			VoterName: votingLogs.voterList1[i],
			candidateName: votingLogs.candiadteList1[i],
		}
		votingDetails.push(votingDetailLog)
	}
	registeredCandidates.data.map((e) => {
		return store.dispatch(setRegisteredCandidate([e.candidate_Address, e.name]))
	})

	whiteListedCandidates[0].forEach((name, index) => {
		const address = whiteListedCandidates[1][index]
		let whiteListedCandidateDetails = [address, name]
		store.dispatch(setWhiteListedCandidate(whiteListedCandidateDetails))
	})

	await store.dispatch(setVotingStatus(votingDetails))
	await store.dispatch(setVotingLog(votingStatus))
}

init()

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<Provider store={store}>
				<App />
			</Provider>
		</BrowserRouter>
	</React.StrictMode>,
	document.getElementById('root')
)

reportWebVitals()
