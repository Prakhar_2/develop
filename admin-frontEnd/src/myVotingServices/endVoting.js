import { contractInstance } from '../index'
import { contract } from '../const/contractDetail'

export const endVoting = async () => {
	try {
		await contractInstance.methods
			.endVoting()
			.send({ from: contract.ownerAddress })
	} catch (e) {
		console.log('Error', e)
	}
}
