import { contractInstance } from '../index'

export const getVoteCount = async () => {
	try {
		const res = await contractInstance.methods.getVoteCount().call()
		return res
	} catch (e) {
		alert('Error', e)
	}
}
