import { contractInstance } from '../index'
import store from '../redux/store/store'
import { setRegisteredCandidate } from '../redux/reducer/RegistredCandidate'

export const registeredCandidateEvent = async () => {
	try {
		await contractInstance.events.registeredCandidateEvent(
			{},
			function (error, event) {
				if (event !== undefined) {
					console.log('event is', event)
					store.dispatch(
						setRegisteredCandidate([
							event.returnValues.candidateAddress,
							event.returnValues.name,
						])
					)
				}
			}
		)
	} catch (e) {
		console.log('error in event ', e)
	}
}
