import { contractInstance } from '../index'
import { contract } from '../const/contractDetail'

export const removeRegistredCandidate = async (candidateAddress) => {
	console.log('candidateAddress is', candidateAddress)
	try {
		await contractInstance.methods
			.removeRegistredCandidate(candidateAddress)
			.send({ from: contract.ownerAddress })
	} catch (e) {
		alert('Error is ', e)
		console.log('error is', e)
	}
}
