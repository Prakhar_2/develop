import { contractInstance } from '../index'

export const voteForCandidate = async (candidateAddress) => {
	try {
		await contractInstance.methods
			.voteForCandidate(candidateAddress)
			.send({ from: window.ethereum.selectedAddress })
	} catch (e) {
		console.log('Error getting', e.message.error)
	}
}
