import { contractInstance } from '../index'
import store from '../redux/store/store'
import { setWhiteListedCandidate } from '../redux/reducer/WhiteListedCandidate'

export const whitelistCandidateEvent = async () => {
	try {
		await contractInstance.events.whiteListedCandidateEvent(
			{},
			function (error, event) {
				if (event !== undefined) {
					store.dispatch(
						setWhiteListedCandidate([
							event.returnValues.candidateAddress,
							event.returnValues.name,
						])
					)
				}
			}
		)
	} catch (e) {
		console.log('error in event ', e)
	}
}
