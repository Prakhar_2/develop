import { createSlice } from '@reduxjs/toolkit'

export const registeredCandidate = createSlice({
	name: 'registeredCandidate',
	initialState: {
		value: [],
	},
	reducers: {
		setRegisteredCandidate: (state, action) => {
			state.value.push(action.payload)
		},
	},
})

export const { setRegisteredCandidate } = registeredCandidate.actions
export default registeredCandidate.reducer
