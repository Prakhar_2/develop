import { createSlice } from '@reduxjs/toolkit'

export const whiteListedCandidate = createSlice({
	name: 'witeListedCandidate',
	initialState: {
		value: [],
	},
	reducers: {
		setWhiteListedCandidate: (state, action) => {
			state.value.push(action.payload)
		},
	},
})

export const { setWhiteListedCandidate } = whiteListedCandidate.actions
export default whiteListedCandidate.reducer
