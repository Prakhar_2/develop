import { createSlice } from '@reduxjs/toolkit'

export const removedRegisteredCandidate = createSlice({
	name: 'removedRegisteredCandidate',
	initialState: {
		value: [],
	},
	reducers: {
		setRemovedRegisteredCandidate: (state, action) => {
			state.value.push(action.payload)
		},
	},
})

export const { setRemovedRegisteredCandidate } =
	removedRegisteredCandidate.actions
export default removedRegisteredCandidate.reducer
