import {
	configureStore,
	combineReducers,
	getDefaultMiddleware,
} from '@reduxjs/toolkit'
import registeredCandidateReducer from '../reducer/RegisteredCandidate'
import whiteListedCandidateReducer from '../reducer/WhiteListedCandidate'
import votingStatusReducer from '../reducer/VotingStatus'
import votingLogReducer from '../reducer/votingLog'
import removedRegisteredCandidateReducer from '../reducer/removedRegisteredCandidate'

const reducer = combineReducers({
	registeredCandidate: registeredCandidateReducer,
	whiteListedCandidate: whiteListedCandidateReducer,
	votingStatus: votingStatusReducer,
	votingLog: votingLogReducer,
	removedRegisteredCandidate: removedRegisteredCandidateReducer,
})

const store = configureStore({
	reducer,
	middleware: [...getDefaultMiddleware({ thunk: false })],
})

export default store
