//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.1;

contract MyVotingSystem {
  struct candidate {
    bool registered;
    bool whitelisted;
    string name;
    uint64 voteCount;
  }

  struct voter {
    bool hasVoted;
    address candiadteAddress;
  }

  address[] candidates;
  address[] voterList;
  address[] regiseredCandidates;
  address public owner;
  bool public isVoting;

  // at end of voting erase mapping//
  mapping(address => candidate) candidateInfo;
  mapping(address => voter) voterInfo;

  constructor() {
    owner = msg.sender;
  }

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  event VoteCountingEvent(
    address indexed voterAddress,
    address indexed candidateAddress
  );

  event whiteListedCandidateEvent(
    string name,
    address indexed candidateAddress
  );
  event votingStatusEvent(bool indexed votingStatus);
  event registeredCandidateEvent(string name, address indexed candidateAddress);
  event getRegisteredCandidateAfterRemoveEvent(
    string name,
    address candidateAddress
  );
  event getWhiteListedCandidateAfterRemoveEvent(
    string name,
    address candidateAddress
  );

  function startVoting() public onlyOwner {
    isVoting = true;
    emit votingStatusEvent(isVoting);
  }

  function showWhiteListed() public view returns (address[] memory) {
    return candidates;
  }

  function isCandidateWhiteListed(address candidateAddress)
    public
    view
    returns (bool)
  {
    return candidateInfo[candidateAddress].whitelisted;
  }

  function isCandidateRegistered(address candidateAddress)
    public
    view
    returns (bool)
  {
    return candidateInfo[candidateAddress].registered;
  }

  function getCandidateDetail(address candidateAddress)
    public
    view
    returns (string memory, uint256)
  {
    string memory name = candidateInfo[candidateAddress].name;
    uint256 voteCount = candidateInfo[candidateAddress].voteCount;
    return (name, voteCount);
  }

  function getReigstredCandidte()
    public
    view
    returns (string[] memory, address[] memory)
  {
    string[] memory RegistredCandidateName = new string[](
      regiseredCandidates.length
    );
    address[] memory RegistredCandidateAddress = new address[](
      regiseredCandidates.length
    );

    for (uint256 i = 0; i < regiseredCandidates.length; i++) {
      RegistredCandidateName[i] = (candidateInfo[regiseredCandidates[i]].name);
      RegistredCandidateAddress[i] = (regiseredCandidates[i]);
    }

    return (RegistredCandidateName, RegistredCandidateAddress);
  }

  function getOwnerAddress() public view returns (address) {
    return owner;
  }

  function getCandidatesAddress() public view returns (address[] memory) {
    return candidates;
  }

  function getVotingStatus()
    public
    view
    returns (address[] memory voterList1, address[] memory candiadteList1)
  {
    address[] memory getCandiadteList = new address[](voterList.length);
    for (uint256 i = 0; i < voterList.length; i++) {
      getCandiadteList[i] = voterInfo[voterList[i]].candiadteAddress;
    }
    return (voterList, getCandiadteList);
  }

  function winner() public view returns (address, string memory) {
    uint256 maxCount;
    string memory Winnername;
    address winnerAddress;

    for (uint256 i = 0; i < candidates.length; i++) {
      if (candidateInfo[candidates[i]].voteCount > maxCount) {
        maxCount = candidateInfo[candidates[i]].voteCount;
        winnerAddress = candidates[i];
        Winnername = candidateInfo[candidates[i]].name;
      }
    }

    return (winnerAddress, Winnername);
  }

  function getTotalVotesforCandidate()
    public
    view
    returns (uint256[] memory, address[] memory)
  {
    uint256[] memory voteCount;
    for (uint256 i = 0; i < candidates.length; i++) {
      voteCount[i] = candidateInfo[candidates[i]].voteCount;
    }

    return (voteCount, candidates);
  }

  function getWhiteListed()
    public
    view
    returns (string[] memory a, address[] memory b)
  {
    string[] memory candidatesName = new string[](candidates.length);

    for (uint256 i = 0; i < candidates.length; i++) {
      candidatesName[i] = candidateInfo[candidates[i]].name;
    }
    return (candidatesName, candidates);
  }

  function getVoteCount()
    public
    view
    returns (
      string[] memory,
      address[] memory,
      uint256[] memory
    )
  {
    uint256[] memory voteCount = new uint256[](candidates.length);
    string[] memory candidateName = new string[](candidates.length);

    for (uint256 i = 0; i < candidates.length; i++) {
      candidateName[i] = candidateInfo[candidates[i]].name;
      voteCount[i] = candidateInfo[candidates[i]].voteCount;
    }
    return (candidateName, candidates, voteCount);
  }

  function addCandidate(string memory name) public {
    require(!isVoting, "Voting started");
    require(
      !candidateInfo[msg.sender].registered,
      "Candidate already registered"
    );

    candidateInfo[msg.sender].name = name;
    candidateInfo[msg.sender].registered = true;
    regiseredCandidates.push(msg.sender);

    emit registeredCandidateEvent(name, msg.sender);
  }

  function removeRegistredCandidate(address candidateAddress) public onlyOwner {
    require(!isVoting, "Voting started");
    require(
      !candidateInfo[candidateAddress].whitelisted,
      "Candidate whitelisted"
    );
    require(
      candidateInfo[candidateAddress].registered,
      "Candiadte  not registred or removed"
    );

    emit getRegisteredCandidateAfterRemoveEvent(
      candidateInfo[candidateAddress].name,
      candidateAddress
    );

    delete candidateInfo[candidateAddress];
  }

  function whitelistCandidate(address candidateAddress) public onlyOwner {
    require(!isVoting, "Voting started");
    require(candidateInfo[candidateAddress].registered, "Not registered");
    require(
      !candidateInfo[candidateAddress].whitelisted,
      "Already whitelisted"
    );

    candidateInfo[candidateAddress].whitelisted = true;
    candidates.push(candidateAddress);
    emit whiteListedCandidateEvent(
      candidateInfo[candidateAddress].name,
      candidateAddress
    );
  }

  function removeWhiteListedCandidate(address candidateAddress)
    public
    onlyOwner
  {
    require(!isVoting, "Voting started");
    require(
      candidateInfo[candidateAddress].whitelisted,
      "Candidate not whitelisted"
    );

    delete candidateInfo[candidateAddress];
    emit getWhiteListedCandidateAfterRemoveEvent(
      candidateInfo[candidateAddress].name,
      candidateAddress
    );
  }

  function voteForCandidate(address candidateAddress) public {
    require(isVoting, "Voting ended");
    require(candidateAddress != address(0), "Zero address");
    require(
      candidateInfo[candidateAddress].whitelisted,
      "Candidate not whitelisted"
    );
    require(!voterInfo[msg.sender].hasVoted, "Has voted");

    candidateInfo[candidateAddress].voteCount += 1;
    voterInfo[msg.sender].hasVoted = true;
    voterList.push(msg.sender);
    voterInfo[msg.sender].candiadteAddress = candidateAddress;
    emit VoteCountingEvent(msg.sender, candidateAddress);
  }

  function endVoting() public onlyOwner {
    isVoting = false;
    emit votingStatusEvent(isVoting);

    for (uint256 i = 0; i < regiseredCandidates.length; i++) {
      candidateInfo[regiseredCandidates[i]].registered = false;
    }

    for (uint256 i = 0; i < candidates.length; i++) {
      candidateInfo[candidates[i]].whitelisted = false;
      candidateInfo[candidates[i]].registered = false;
      delete candidateInfo[candidates[i]];
    }

    for (uint256 i = 0; i < voterList.length; i++) {
      voterInfo[voterList[i]].hasVoted = false;
      delete voterInfo[voterList[i]];
    }

    delete voterList;
    delete regiseredCandidates;
    delete candidates;
  }
}
