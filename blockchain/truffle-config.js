module.exports = {
  networks: {
    development: {
     host: "localhost",
     port: 7545,
     network_id: "*",
    }
  },

  compilers: {
    solc: {
      version: "0.8.1",
      // docker: true,
      settings: {
       optimizer: {
         enabled: false,
         runs: 200
       },
       evmVersion: "byzantium"
      }
    }
  },

  db: {
    enabled: false
  }
};
