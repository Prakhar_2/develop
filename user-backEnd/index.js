/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-console */
import express from 'express'
import cors from 'cors'
import dotEnv from 'dotenv'
import bodyParser from 'body-parser'
import { Server } from 'socket.io'
import chalk from 'chalk'
import routes from './src/routes/routes'
import db from './src/db/models'
import VoteCountingEvent from './src/listeners/voteCountingEvent'

dotEnv.config()
const success = chalk.greenBright
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use('/', routes)

const PORT = process.env.PORT || 9000

const server = app.listen(PORT, async () => {
	await db.sequelize.sync()
	// eslint-disable-next-line no-console
	console.log(success(`listing on port ${PORT}`))
})

const io = new Server().listen(server)

io.on('connection', async (socket) => {
	console.log('New client connected at user End', socket.id)
	await VoteCountingEvent(socket)
})
