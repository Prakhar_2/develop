const config = {
	NODE_ENV: 'development',

	development: {
		host: '127.0.0.1',
		web3Url: 'http://0.0.0.0:7545',
		contractAddress: '0x034B51A96187540318520294A05dF3B82726A504',
		PORT: '8000',
	},
	test: {
		username: 'test',
		password: 1234,
		database: 'testing_purpose',
		host: '127.0.0.1',
		dialect: 'postgres',
	},
	production: {
		username: 'test',
		password: 1234,
		database: 'testing_purpose',
		host: '127.0.0.1',
		dialect: 'postgres',
	},
}

export default config[config.NODE_ENV]
