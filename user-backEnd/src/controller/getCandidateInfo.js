import chalk from 'chalk'
import { candidateDetails } from '../db/models'

const error = chalk.bold.red

const getCandidateDetails = async (req, res) => {
	try {
		const { candidateAddress } = req.body
		const candiadteInfo = await candidateDetails.findByPk(candidateAddress)

		if (candiadteInfo === null) {
			res.send({ error: 'No such candidate' })
		} else {
			const logs = { 0: candiadteInfo.name, 1: candiadteInfo.vote_Count }
			res.send(logs)
		}
	} catch (e) {
		res.status(400).send(error('ERROR'))
	}
}

export default getCandidateDetails
