import chalk from 'chalk'
import { registeredCandidateDetails } from '../db/models'

const ERROR = chalk.bold.red

const getVotingLog = async (req, res) => {
	try {
		const RegisteredCandidatesLogs = await registeredCandidateDetails.findAll()
		res.send(RegisteredCandidatesLogs)
	} catch (e) {
		res.status(400).send(ERROR('error is', e))
	}
}

export default getVotingLog
