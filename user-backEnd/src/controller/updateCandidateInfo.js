import { candidateDetails } from '../db/models'

const getCandidateDetails = async (req, res) => {
	try {
		const { candidateAddress, name, voteCount } = req.body
		const candiadteInfo = await candidateDetails.findByPk(candidateAddress)

		if (candiadteInfo === null) {
			await candidateDetails.create({
				candidate_Address: candidateAddress,
				name,
				vote_Count: voteCount,
			})
		} else {
			await candidateDetails.update(
				{
					name,
					vote_Count: voteCount,
				},
				{
					where: { candidate_Address: candidateAddress },
				}
			)
		}
		await res.send(candiadteInfo)
	} catch (e) {
		res.status(400).send('ERROR')
	}
}

export default getCandidateDetails
