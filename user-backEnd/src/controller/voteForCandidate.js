/* eslint-disable no-console */
import voteForCandidate from '../../../user-backEnd/src/services/myVotingServices /voteForCandidate'

const voteForSelectedCandidate = async (req, res) => {
	try {
		const { candidateAddress, voterAddress } = req.body
		console.log('in server is ', candidateAddress, voterAddress)
		await voteForCandidate(candidateAddress, voterAddress)
	} catch (e) {
		res.status(400).send('ERROR', e)
	}
}

export default voteForSelectedCandidate
