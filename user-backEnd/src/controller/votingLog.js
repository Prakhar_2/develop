import getVotingStatus from '../services/myVotingServices /getVotingStatus'
import { votingLogDetails } from '../db/models'

const votingLog = async (req, res) => {
	try {
		const result = await votingLogDetails.findAll()

		if (result.length === 0) {
			const logs = await getVotingStatus()
			logs.voterList1.forEach(async (ele, index) => {
				const voterAddress = ele
				const candidateAddress = logs.candiadteList1[index]
				await votingLogDetails.create({
					candidate_Address: candidateAddress,
					voter_Address: voterAddress,
				})
			})

			const result1 = await votingLogDetails.findAll()
			res.send(result1)
		} else {
			res.send(result)
		}
	} catch (e) {
		res.status(400).send('ERROR')
	}
}

export default votingLog
