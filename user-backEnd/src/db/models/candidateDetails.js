module.exports = (sequelize, DataTypes) => {
	const CandidateDetails = sequelize.define('candidateDetails', {
		candidate_Address: {
			type: DataTypes.STRING,
			allowNull: false,
			primaryKey: true,
		},

		name: {
			type: DataTypes.STRING,
			allowNull: false,
		},

		vote_Count: {
			type: DataTypes.INTEGER,
			allowNull: false,
		},
		registered: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
		whiteListed: {
			type: DataTypes.INTEGER,
			allowNull: true,
		},
	})
	return CandidateDetails
}
