import express from 'express'
import votingLog from '../controller/votingLog'
import getCandidateInfo from '../controller/getCandidateInfo'
import voteForCandidate from '../controller/voteForCandidate'
import updateCandidateInfo from '../controller/updateCandidateInfo'
import previousVotingLog from '../controller/previousVotingLog'

const Router = express.Router()

Router.get('/data', votingLog)
Router.get('/previousVotingLog', previousVotingLog)
Router.post('/candidateInfo', getCandidateInfo)
Router.post('/voteCandiadte', voteForCandidate)
Router.post('/updateCandidateInfo', updateCandidateInfo)

export default Router
