const contractInstance = require('../const/contractDetail')

const getAdmin = async () => {
	try {
		const result = await contractInstance.methods.getAdmin.call().call()
		console.log('result is ', result)
		return result
	} catch (e) {
		console.log('error in getting admin', e)
	}
}
module.exports = getAdmin
