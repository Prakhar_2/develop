/* eslint-disable consistent-return */
import contractInstance from '../../lib/contract'

const getParticularCandidateDetail = async (candidateAddress) => {
	try {
		const res = await contractInstance.methods
			.getCandidateDetail(candidateAddress)
			.call()
		return res
	} catch (e) {
		// eslint-disable-next-line no-console
		console.log('Error in getting particular candidate', e)
	}
}

export default getParticularCandidateDetail
