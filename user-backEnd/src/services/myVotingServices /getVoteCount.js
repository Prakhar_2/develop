const contractInstance = require('../const/contractDetail')

const getVoteCount = async () => {
	try {
		const res = await contractInstance.methods.getVoteCount().call()
		return res
	} catch (e) {
		console.log('Error', e)
	}
}

module.exports = getVoteCount
