import { contractInstance } from '../index'
import { contract } from '../const/contractDetail'

export const removeRegistredCandidate = async (candidateAddress) => {
	try {
		await contractInstance.methods
			.removeRegistredCandidate(candidateAddress)
			.send({ from: contract.ownerAddress })
	} catch (e) {
		alert('Error is ', e)
	}
}
