import { contractInstance } from '../index'

export const showWhiteListed = async () => {
	try {
		const result = await contractInstance.methods.showWhiteListed().call()
		return result
	} catch (e) {
		console.log('Error is ', e)
	}
}
