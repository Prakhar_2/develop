const contractInstance = require('../const/contractDetail')
const store = require('../redux/store/store')
const { setVotingLog } = require('../redux/reducer/votingLog')

export const votingStatusEvent = async () => {
	try {
		await contractInstance.events.votingStatus({}, function (error, event) {
			if (event !== undefined) {
				store.dispatch(setVotingLog(event.returnValues.votingStatus))
			}
		})
	} catch (e) {
		console.log('error in event ', e)
	}
}
