import { contractInstance } from '../index'
import { contract } from '../const/contractDetail'

export const whitelistCandidate = async (candidateAddress) => {
	try {
		await contractInstance.methods
			.whitelistCandidate(candidateAddress)
			.send({ from: contract.ownerAddress })
	} catch (e) {
		alert('Error', e)
	}
}
