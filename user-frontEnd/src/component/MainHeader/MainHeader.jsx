import React from 'react'
import { Link } from 'react-router-dom'
import Indicator from './Indicator/Indicator'
import { BsFillPersonCheckFill } from 'react-icons/bs'
import { GiVote } from 'react-icons/gi'
import { Typography } from '@material-ui/core'
import './style.css'

const MainHeader = () => {
	return (
		<div>
			<div style={{ display: 'flex' }}>
				<Link
					to='/Candidate'
					style={{
						marginLeft: '1Vw',
						fontSize: '40px',
						color: '#ff9100',
						textDecoration: 'none',
					}}
				>
					<div className='a' style={{ display: 'flex', marginTop: '1Vh' }}>
						<BsFillPersonCheckFill
							style={{ fontSize: '40px', marginTop: '1Vh' }}
						/>
						<Typography style={{ marginLeft: '1Vw', fontSize: '40px' }}>
							Candidate
						</Typography>
					</div>
				</Link>

				<Link
					to='/Voter'
					style={{
						marginLeft: '1Vw',
						fontSize: '40px',
						color: '#ff9100',
						textDecoration: 'none',
					}}
				>
					<div className='a' style={{ display: 'flex', marginTop: '1Vh' }}>
						<GiVote style={{ fontSize: '40px', marginTop: '1Vh' }} />
						<Typography style={{ marginLeft: '1Vw', fontSize: '40px' }}>
							Voter
						</Typography>
					</div>
				</Link>

				<div
					style={{
						display: 'flex',
						marginLeft: 'auto',
						marginRight: '5%',
						marginTop: '3Vh',
					}}
				>
					<Typography
						style={{ fontSize: '20px', fontWeight: '900', color: '#3f51b5' }}
					>
						voting Status{' '}
					</Typography>
					<Indicator />
				</div>
			</div>
		</div>
	)
}

export default MainHeader
