import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Fade, Backdrop } from '@material-ui/core'
import useStyles from './style'

const FadeModal = ({ children, isOpen, hide }) => {
	const classes = useStyles()

	return (
		<Modal
			aria-labelledby='modal-title'
			aria-describedby='modal-description'
			open={isOpen}
			onClose={hide}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 500,
			}}
			className={classes.modal}
		>
			<Fade in={isOpen} style={{ outline: 'none' }}>
				{children}
			</Fade>
		</Modal>
	)
}

FadeModal.defaultProps = {
	children: <> </>,
	isOpen: false,
	hide: () => {},
}

FadeModal.propTypes = {
	children: PropTypes.element,
	isOpen: PropTypes.bool,
	hide: PropTypes.func,
}

export default FadeModal
