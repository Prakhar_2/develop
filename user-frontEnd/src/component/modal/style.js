import { makeStyles } from '@material-ui/core'

export default makeStyles(() => ({
	modal: {
		marginTop: '15%',
		width: '50%',
		marginLeft: '20%',
	},
}))
