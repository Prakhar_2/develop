import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import axios from 'axios'
import { getParticularCandidateDetail } from '../../myVotingServices /getParticularCandidateDetail'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const Tables = ({ buttonText, onButtonClick, whiteListedDetails }) => {
	const useStyles = makeStyles({
		table: {
			minWidth: 500,
			maxWidth: 700,
			marginTop: '5%',
		},
	})

	const classes = useStyles()

	const handleClick = async (index) => {
		try {
			await onButtonClick(whiteListedDetails[index].address)
			const candidateDetails = await getParticularCandidateDetail(
				whiteListedDetails[index].address
			)
			// console.log('candidateDetails are ', candidateDetails)
			axios.post('/updateCandidateInfo', {
				candidateAddress: whiteListedDetails[index].address,
				name: candidateDetails[0],
				voteCount: candidateDetails[1],
			})
		} catch (e) {
			alert('You can not do any thing', e)
		}
	}

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Name
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Address
						</StyledTableCell>
						<StyledTableCell
							style={{ textAlign: 'left' }}
							key={Math.random()}
						></StyledTableCell>
					</StyledTableRow>
				</TableHead>

				<TableBody>
					{whiteListedDetails.map((ele, index) => {
						return (
							<StyledTableRow data-index={index}>
								<StyledTableCell key={ele.name}>{ele.name}</StyledTableCell>
								<StyledTableCell key={ele.address} id={index}>
									{ele.address}
								</StyledTableCell>
								<div style={{ display: 'flex' }}>
									<StyledTableCell key={Math.random()}>
										{
											<button
												onClick={() => {
													handleClick(index)
												}}
												className={classes.buttons}
											>
												{buttonText}
											</button>
										}
									</StyledTableCell>
								</div>
							</StyledTableRow>
						)
					})}
				</TableBody>
			</Table>
		</div>
	)
}

export default Tables
