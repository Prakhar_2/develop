import React from 'react'
import { useSelector } from 'react-redux'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { removeRegistredCandidate } from '../../../myVotingServices /removeRegisteredCandidate'
import { whitelistCandidate } from '../../../myVotingServices /whiteListCandidate'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const useStyles = makeStyles({
	table: {
		minWidth: 500,
		maxWidth: 700,
		marginTop: '5%',
	},
})

const RegisteredCandidate = () => {
	const classes = useStyles()

	const candidateName = []
	const candidateAddress = []
	const registeredCandidate = useSelector(
		(state) => state.registeredCandidate.value
	)

	for (let registeredCandidateDetail of registeredCandidate) {
		const { name } = registeredCandidateDetail
		const { address } = registeredCandidateDetail
		candidateName.push(name)
		candidateAddress.push(address)
	}

	const handleClick = async (index) => {
		await removeRegistredCandidate(candidateAddress[index])
	}

	const handleWhiteList = async (index) => {
		await whitelistCandidate(candidateAddress[index])
	}

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Name
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Address
						</StyledTableCell>
						<StyledTableCell style={{ textAlign: 'left' }} key={Math.random()}>
							Registered
						</StyledTableCell>
					</StyledTableRow>
				</TableHead>

				<TableBody>
					{candidateName.map((ele, index) => (
						<StyledTableRow data-index={index}>
							<StyledTableCell key={ele}>{ele}</StyledTableCell>
							<StyledTableCell
								key={candidateAddress[index]}
								id={candidateAddress[index]}
							>
								{candidateAddress[index]}
							</StyledTableCell>

							<div
								style={{
									display: 'flex',
									marginTop: '2Vh',
									textAlign: 'center',
								}}
							>
								<button
									onClick={() => {
										handleClick(index)
									}}
								>
									remove
								</button>
								<button
									onClick={() => {
										handleWhiteList(index)
									}}
									style={{ marginLeft: '1Vw', marginRight: '1Vw' }}
								>
									whiteList
								</button>
							</div>
						</StyledTableRow>
					))}
				</TableBody>
			</Table>
		</div>
	)
}

export default RegisteredCandidate
