import React from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { useSelector } from 'react-redux'

const VotingStatus = () => {
	const StyledTableCell = withStyles((theme) => ({
		head: {
			backgroundColor: '#ff5722',
			color: theme.palette.common.white,
			textAlign: 'left',
			marginTop: '3Vw',
		},
		body: {
			fontSize: '12.5px',
		},
	}))(TableCell)

	const StyledTableRow = withStyles((theme) => ({
		root: {
			'&:nth-of-type(odd)': {
				backgroundColor: 'grey',
			},
			backgroundColor: 'rgb(115, 143, 189)',
		},
	}))(TableRow)

	const useStyles = makeStyles({
		table: {
			minWidth: 500,
			maxWidth: 700,
			marginTop: '5%',
		},
	})

	const classes = useStyles()

	let voteCountLogs = []
	const votingState = useSelector((state) => state.votingStatus.value)

	for (let voteLog of votingState) {
		for (let detail of voteLog) voteCountLogs.push(detail)
	}

	return (
		<div>
			<Table className={classes.table}>
				<TableHead>
					<StyledTableRow>
						<StyledTableCell
							style={{ textAlign: 'center' }}
							key={Math.random()}
						>
							{' '}
							from
						</StyledTableCell>
						<StyledTableCell
							style={{ textAlign: 'center' }}
							key={Math.random()}
						>
							{' '}
							to{' '}
						</StyledTableCell>
					</StyledTableRow>
				</TableHead>

				<TableBody>
					{voteCountLogs.map((ele, index) => {
						return (
							<StyledTableRow>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									{ele.voterAddress}
								</StyledTableCell>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									{ele.candidateAddress}
								</StyledTableCell>
							</StyledTableRow>
						)
					})}
				</TableBody>
			</Table>
		</div>
	)
}

export default VotingStatus
