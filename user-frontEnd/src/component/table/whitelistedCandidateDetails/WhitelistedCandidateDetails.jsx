import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import { withStyles } from '@material-ui/core/styles'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import useStyles1 from './style'

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#ff5722',
		color: theme.palette.common.white,
		textAlign: 'left',
		marginTop: '3Vw',
	},
	body: {
		fontSize: '20px',
	},
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: 'grey',
		},
		backgroundColor: 'rgb(115, 143, 189)',
	},
}))(TableRow)

const WhitelistedCandidateDetails = ({ name, address, voteCount }) => {
	const classes = useStyles1()

	return (
		<div>
			<Card className={classes.card}>
				<Table className={classes.table}>
					<CardContent>
						<TableHead>
							<StyledTableRow>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									Name
								</StyledTableCell>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									Address
								</StyledTableCell>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									VoteCount
								</StyledTableCell>
							</StyledTableRow>
						</TableHead>

						<TableBody>
							<StyledTableRow>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									{name}
								</StyledTableCell>
								<StyledTableCell
									style={{ textAlign: 'left' }}
									key={Math.random()}
								>
									{address}
								</StyledTableCell>
								<StyledTableCell
									style={{ textAlign: 'center' }}
									key={voteCount}
								>
									{voteCount}
								</StyledTableCell>
							</StyledTableRow>
						</TableBody>
					</CardContent>
				</Table>
			</Card>
		</div>
	)
}

export default WhitelistedCandidateDetails
