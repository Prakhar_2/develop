import { makeStyles } from '@material-ui/core/styles'

const useStyles1 = makeStyles((theme) => ({
	card: {
		width: '80%',
		backgroundColor: 'black',
		margin: '5Vw',
	},
	table: {
		margin: '2Vw',
		marginLeft: '4Vw',
	},
}))

export default useStyles1
