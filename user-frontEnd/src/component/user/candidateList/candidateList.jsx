import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Box, Typography } from '@material-ui/core'
import { BiStats } from 'react-icons/bi'
import { useStyles } from './style'
import SignUpModal from '../../user/candidateList/signUp/signUpModal'
import { Button, withStyles } from '@material-ui/core'
import { blue, green } from '@material-ui/core/colors'
import axios from 'axios'
import Winner from '../../winner/winner'
import '../../../text.css'
import VotingStatus from '../../table/votingStatus/VotingStatusUpdate'
import { useSelector } from 'react-redux'
import MainHeader from '../../MainHeader/MainHeader'
import ShowStatus from './showStatus/ShowStatus'
import WhitelistedCandidateDetails from '../../table/whitelistedCandidateDetails/WhitelistedCandidateDetails'

const Item = (props) => {
	const { sx, ...other } = props
	return <Box {...other} />
}

Item.propTypes = {
	sx: PropTypes.object,
}

const ToggleButton = (props) => {
	return (
		<>
			<div>
				<ColorButton
					{...props.className}
					size='large'
					variant='contained'
					color='primary'
					onClick={props.onClickSignUp}
				>
					<Typography
						style={{
							fontFamily: 'fantasy',
							color: 'whitesmoke',
							fontWeight: '900',
							fontSize: '40px',
						}}
					>
						SIGNUP YOURSELF AS A CANDIDATE !!{' '}
					</Typography>
				</ColorButton>
			</div>
		</>
	)
}

const ColorButton = withStyles((theme) => ({
	root: {
		color: theme.palette.getContrastText(green[500]),
		size: 'large',
		marginTop: '9Vh',
		minWidth: '40rem',
		maxWidth: '90rem',
		margin: theme.spacing(2),
		backgroundColor: green[700],
		'&:hover': {
			backgroundColor: blue[600],
		},
	},
}))(Button)

const CandidateList = () => {
	let votingStarted = useSelector((state) => state.votingLog.value)
	const [openSignUp, setOpenSignUp] = useState(false)
	let selectedAddressIs = ''

	if (window.ethereum.selectedAddress !== null) {
		selectedAddressIs = window.ethereum.selectedAddress
	}

	const [selectedAddress, setSelectedAddress] = useState(selectedAddressIs)
	const [candidateName, setCandidateName] = useState('')
	const [candidateAddressVoteCount, setSelectedAddressVoteCount] = useState()

	window.ethereum.on('accountsChanged', (accounts) => {
		setSelectedAddress(accounts[accounts.length - 1])
	})

	useEffect(() => {
		const init = async () => {
			const candidateDetails = await axios.post('/candidateInfo', {
				candidateAddress: selectedAddress,
			})
			if (candidateDetails !== undefined) {
				setCandidateName(candidateDetails.data[0])
				setSelectedAddressVoteCount(candidateDetails.data[1])
			}
		}

		init()
	})

	const classes = useStyles()

	return (
		<div>
			<div className='sticky'>
				<MainHeader />
			</div>
			<Box
				style={{
					display: 'grid',
					gridAutoFlow: 'row',
					gridTemplateColumns: 'repeat(2, 60% 40%)',
					gridTemplateRows: 'repeat(1, 100Vh)',
					backgroundColor: 'black',
					gap: '20px',
					marginTop: '6Vh',
				}}
			>
				<Item style={{ gridRow: '1', backgroundColor: '#ccfff5' }}>
					{votingStarted === true ? (
						<>
							<h3 className={classes.typo1}>Whitelisted candidate Details </h3>
							<WhitelistedCandidateDetails
								name={candidateName}
								address={selectedAddress}
								voteCount={candidateAddressVoteCount}
							/>
						</>
					) : (
						<>
							<ToggleButton onClickSignUp={() => setOpenSignUp(true)} />
							<SignUpModal
								open={openSignUp}
								handleClose={() => setOpenSignUp(false)}
							/>
							<>
								<div
									style={{
										marginTop: '9rem',
										display: 'flex',
										marginLeft: '40px',
									}}
									className='a'
								>
									<BiStats style={{ fontSize: '80px' }} />
									<Typography
										style={{
											marginLeft: '4px',
											marginRight: '5%',
											fontSize: '50px',
											fontWeight: '600',
										}}
									>
										Nomination Status :--
									</Typography>
								</div>
								<ShowStatus candidateAddress={selectedAddress} />
							</>
						</>
					)}
				</Item>

				<Item style={{ backgroundColor: '#0f0e0e', width: '97%' }}>
					{votingStarted === true ? (
						<div style={{ marginLeft: '1Vw' }}>
							<div style={{ width: '80%', marginLeft: '10%' }}>
								<Winner />
							</div>

							<div style={{ marginLeft: '0Vw', marginRight: '2Vw' }}>
								<VotingStatus />
							</div>
						</div>
					) : (
						<div className='fo'>
							<h1 className='a'>wait till voting being starts... </h1>
						</div>
					)}
				</Item>
			</Box>
		</div>
	)
}

export default CandidateList
