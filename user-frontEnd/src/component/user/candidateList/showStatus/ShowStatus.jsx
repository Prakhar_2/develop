import React, { useState, useEffect } from 'react'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import Button from '@material-ui/core/Button'
import './showStatus.css'
import { useSelector } from 'react-redux'

const ShowStatus = ({ candidateAddress }) => {
	const registeredCandidate = useSelector(
		(state) => state.registeredCandidate.value
	)

	const removedRegisteredCandidate = useSelector(
		(state) => state.removedRegisteredCandidate.value
	)

	const whiteListedCandidatesLogs = useSelector(
		(state) => state.whiteListedCandidate.whiteListedCandidates
	)

	const [registered, setRegistered] = useState(false)
	const [removeRegistered, setRemoveRegistered] = useState(false)
	const [whiteListed, setWhiteListed] = useState(false)

	useEffect(() => {
		const init = () => {
			for (let registeredInfo of registeredCandidate) {
				if (registeredInfo[0][0].toLowerCase() === candidateAddress) {
					setRegistered(true)
					break
				}
			}

			const uniqueRemovedRegisteredCandidates = new Set()
			for (let removed of removedRegisteredCandidate) {
				uniqueRemovedRegisteredCandidates.add(removed[0])
			}

			for (let item of uniqueRemovedRegisteredCandidates) {
				if (item.toLowerCase() === candidateAddress) {
					setRemoveRegistered(true)
					break
				}
			}

			const uniqueWhiteListedRegisteredCandidates = new Set()
			for (let whiteListed of whiteListedCandidatesLogs) {
				uniqueWhiteListedRegisteredCandidates.add(whiteListed[0])
			}

			for (let item of uniqueWhiteListedRegisteredCandidates) {
				if (item.toLowerCase() === candidateAddress) {
					setWhiteListed(true)
					break
				}
			}
		}
		init()

		return () => {
			setRegistered(false)
			setRemoveRegistered(false)
			setWhiteListed(false)
		}
	}, [
		candidateAddress,
		registeredCandidate,
		removeRegistered,
		removedRegisteredCandidate,
		registered,
		whiteListed,
		whiteListedCandidatesLogs,
	])

	return (
		<div div className='container1'>
			<ButtonGroup
				disableElevation
				variant='contained'
				color='white'
				style={{ marginTop: '9%' }}
			>
				{registered === true ? (
					<Button style={{ backgroundColor: 'green', fontSize: '3rem' }}>
						Registered
					</Button>
				) : (
					<Button variant='contained' style={{ fontSize: '3rem' }}>
						Registered
					</Button>
				)}

				{removeRegistered === true ? (
					<Button style={{ backgroundColor: 'red', fontSize: '3rem' }}>
						Rejected
					</Button>
				) : (
					<Button variant='contained' style={{ backgroundColor: 'yellow' }}>
						Pending
					</Button>
				)}

				{whiteListed === true ? (
					<Button style={{ backgroundColor: 'green', fontSize: '3rem' }}>
						WhiteListed
					</Button>
				) : (
					<></>
				)}
			</ButtonGroup>
		</div>
	)
}

export default ShowStatus
