import React from 'react'
import { useFormik } from 'formik'
import { useStyles } from './style'
import * as Yup from 'yup'
import { addCandidate } from '../../../../../myVotingServices /addCandidate'
import TextField from '@material-ui/core/TextField'
import { FaUserCircle } from 'react-icons/fa'
import '../../../../../text.css'

const SignUpForm = ({ votingStarted }) => {
	const classes = useStyles()

	const formik = useFormik({
		initialValues: {
			Name: '',
		},
		validationSchema: Yup.object({
			Name: Yup.string()
				.min(2, 'Candidate name must be more than 2 character')
				.max(15, 'Must be 15 characters or less')
				.matches(/^[aA-zZ\s]+$/, 'Name should be character only')
				.required('Required'),
		}),
		onSubmit: async (values) => {
			let candidateName = values.Name
			candidateName = candidateName.trim()
			if (!candidateName.length) {
				alert('Candidate name should not be empty')
			}
			if (!votingStarted && candidateName.length) {
				await addCandidate(candidateName)
			}
		},
	})

	return (
		<div className={classes.div}>
			<form className={classes.form} onSubmit={formik.handleSubmit}>
				<div style={{ display: 'flex', padding: '1.5Vw' }}>
					<FaUserCircle className='logo' />
					<TextField
						className={classes.input}
						id='CandidateName'
						type='CandidateName'
						style={{ width: '50%' }}
						{...formik.getFieldProps('Name')}
						label='CandidateName'
					/>
				</div>
				{formik.touched.Name && formik.errors.Name ? (
					<div className={classes.error}>{formik.errors.Name}</div>
				) : null}
				<button className='button' type='submit'>
					Register
				</button>
			</form>
		</div>
	)
}

export default SignUpForm
