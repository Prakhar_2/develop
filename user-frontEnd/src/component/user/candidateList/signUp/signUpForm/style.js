import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
	form: {
		borderRadius: '2rem',
		alignItem: 'center',
		borderColor: 'yellow',
	},
	image1: {
		width: '45%',
		marginLeft: '-3Vw',
	},
	input: {
		padding: '.5rem',
		fontSize: '16px',
		marginLeft: '3Vw',
		width: '40%',
	},
	label: {
		fontWeight: 'bold',
		display: 'block',
		marginTop: '2px',
		fontSize: '30px',
		color: 'blue',
	},
	div: {
		marginLeft: '0%',
		marginTop: '0Vh',
		marginRight: '0%',
	},
	error: {
		color: 'red',
		marginLeft: '1Vw',
		marginBottom: '2Vw',
		fontSize: '1.7Vw',
	},
	Dialog: {
		backgroundColor: 'rgb(60, 179, 113)',
		justifyItems: 'center',
	},
	text1: {
		color: '#ff6633',
		fontSize: '20px',
		marginLeft: '5px',
	},
}))
