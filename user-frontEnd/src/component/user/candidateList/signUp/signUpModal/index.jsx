import React from 'react'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import { Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { useStyles } from './style'
import SignUpModalTitle from '../signUpTitle/signUpTitle'
import SignUpForm from '../signUpForm/signUpForm'
import FadeModal from '../../../../modal/index'
import useStyles1 from './style1'
import { useSelector } from 'react-redux'

const DialogTitle = withStyles(useStyles1)((props1) => {
	const { children, classes, onClose, ...other } = props1

	return (
		<MuiDialogTitle disableTypography {...other}>
			<Typography variant='h6'>{children}</Typography>
		</MuiDialogTitle>
	)
})
const DialogContent = withStyles((theme) => ({
	root: {
		padding: theme.spacing(2),
	},
}))(MuiDialogContent)

const SignupModal = (props) => {
	const classes = useStyles()
	const votingStarted = useSelector((state) => state.votingLog.value)

	useStyles1()

	return (
		<div>
			<FadeModal isOpen={props.open} hide={props.handleClose}>
				<>
					<>
						<DialogTitle className={classes.Dialog} onClose={props.handleClose}>
							<SignUpModalTitle />
						</DialogTitle>
					</>

					<>
						<DialogContent className={classes.Dialog} dividers>
							<SignUpForm votingStarted={votingStarted} />
						</DialogContent>
					</>
				</>
			</FadeModal>
		</div>
	)
}

export default SignupModal
