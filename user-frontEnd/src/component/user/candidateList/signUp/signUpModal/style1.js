import { makeStyles } from '@material-ui/core/styles'

const useStyles1 = makeStyles((theme) => ({
	root: {
		margin: 0,
		padding: theme.spacing(2),
	},
	customizeToolbar: {
		minHeight: 36,
	},
	closeButton: {
		position: 'absolute',
		right: theme.spacing(1),
		top: theme.spacing(1),
		color: theme.palette.grey[500],
	},
	extra: {
		color: 'white',
		marginLeft: '310px',
	},
	root1: {
		flexGrow: 1,
		boxShadow: 'none',
		backgroundColor: '#141452',
	},
	toolbar: {
		paddingRight: 0,
	},
	home: {
		position: 'relative',
	},
	Space: {
		margin: theme.spacing(1),
	},
	modal: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	text: {
		color: 'white',
	},
	text1: {
		color: '#ff6633',
		fontSize: '20px',
		marginLeft: '5px',
	},
	Dialog: {
		backgroundColor: '#000033',
		justifyItems: 'center',
	},
	loginTypo: {
		fontSize: '15px',
		color: 'white',
	},
	MARGIN: {
		margin: theme.spacing(1),
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
	image: {
		marginRight: '5px',
		marginTop: '15px',
		marginLeft: '15px',
	},
	divMargin: {
		marginLeft: '25px',
	},
	div1margin: {
		marginLeft: '12px',
	},
	back: {
		backgroundColor: '#ff6633',
	},

	ee: {
		color: 'white',
	},
	Active: {
		fontWeight: 'bold',
		color: '#ffb366',
	},

	active: {
		paddingBottom: '10px',
		borderBottom: '3px solid #19A5B8',
	},
	navlink: {
		color: 'white',
		textDecoration: 'none',
		paddingRight: '5px',
		paddingTop: '15px',
		paddingLeft: '5px',
		fontFamily: 'Sans-serif',
	},
}))
export default useStyles1
