import { Typography } from '@material-ui/core'
import React from 'react'
import Registration from '../../../../../asset/registration.png'
import '../../../../../text.css'

const SignUpModalTitle = () => {
	return (
		<div style={{ display: 'flex' }}>
			<img className='image' src={Registration} alt='P' />
			<Typography
				className='b'
				style={{
					fontFamily: 'fantasy',
					color: 'whitesmoke',
					fontWeight: '900',
					marginTop: '7Vh',
					marginLeft: '-3Vh',
					fontSize: '40px',
				}}
			>
				REGISTER YOURSELF !
			</Typography>
		</div>
	)
}

export default SignUpModalTitle
