import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
	body: {
		marginLeft: '0Vw',
		marginRight: '0Vw',
		marginBottom: '0Vh',
		marginTop: '0Vh',
	},
	typo: {
		color: '#99ff99',
		marginLeft: '20%',
		textDecoration: 'underline',
		marginTop: '4Vh',
		fontSize: '2rem',
	},
	typo1: {
		color: 'green',
		marginLeft: '5%',
		textDecoration: 'underline',
		marginTop: '4Vh',
		fontSize: '4rem',
	},
}))
