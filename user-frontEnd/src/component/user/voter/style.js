import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(() => ({
	body: {
		position: 'absolute',
	},
	body1: {
		position: 'relative',
	},
	bgColor: {
		backgroundColor: 'black',
		rowSpan: 4,
	},
	formControl: {
		marginLeft: '10Vw',
		marginRight: '35Vw',
		marginTop: '5Vh',
		marginBottom: '10Vw',
		width: 200,
	},
	button: {
		marginLeft: '1Vw',
		marginRight: '0px',
		marginTop: '10Vh',
		marginBottom: '0px',
	},
	box: {
		width: '80%',
		height: 'auto',
		backgroundColor: 'black',
		alignItems: 'center',
		borderRadius: '3px',
		borderStyle: 'solid',
		borderColor: 'black',
		marginLeft: '8%',
		marginRight: '10%',
		marginTop: '5Vh',
		marginBottom: '0px',
	},
	heading: {
		marginLeft: '30%',
		marginRight: '30%',
		marginTop: '5Vw',
		color: 'floralwhite',
	},
	heading1: {
		marginLeft: '15%',
		marginRight: '10%',
		marginTop: '4Vh',
		textDecoration: ' underline',
		color: 'red',
		fontSize: '4rem',
	},
	button1: {
		marginTop: '2Vh',
		color: 'black',
		size: '20px',
		backgroundColor: 'green',
	},
	typo: {
		color: '#99ff99',
		marginLeft: '20%',
		textDecoration: 'underline',
		marginTop: '4Vh',
		fontSize: '2rem',
	},
}))
