import * as React from 'react'
import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Box, Button } from '@material-ui/core'
import { useStyles } from './style'
import { getWhiteListed } from '../../../myVotingServices /getWhiteListed'
import { voteForCandidate } from '../../../myVotingServices /voteForCandidate'
import Table from '../../table/Table'
import Winner from '../../winner/winner'
import VotingStatus from '../../table/votingStatus/VotingStatusUpdate'
import { useSelector } from 'react-redux'
import '../../../text.css'
import MainHeader from '../../MainHeader/MainHeader'
import { SnackbarProvider } from 'notistack'

const Item = (props) => {
	const { sx, ...other } = props
	return <Box {...other} />
}

Item.propTypes = {
	sx: PropTypes.object,
}

const Voter = () => {
	const classes = useStyles()

	let votingStarted = useSelector((state) => state.votingLog.value)
	const [clicked, setClick] = useState(false)
	const [whiteListedAddress, setWhiteListedAddress] = useState([])
	const [names, setWhiteListedNames] = useState([])

	const handleClick = async () => {
		setClick(true)
		const res = await getWhiteListed()
		setWhiteListedNames(res[0])
		setWhiteListedAddress(res[1])
	}

	let whiteListedDetails = []

	names.forEach((name, index) => {
		const address = whiteListedAddress[index]
		let detail = { name: name, address: address }
		whiteListedDetails.push(detail)
	})

	useEffect(() => {
		const init = async () => {
			const whiteListedCandidates = await getWhiteListed()
			setWhiteListedNames(whiteListedCandidates[0])
			setWhiteListedAddress(whiteListedCandidates[1])
		}

		init()
	}, [votingStarted])

	return (
		<>
			<div style={{ width: '99%' }}>
				<div className='sticky'>
					<MainHeader />
				</div>
				<Box
					style={{
						display: 'grid',
						gridAutoFlow: 'row',
						gridTemplateColumns: 'repeat(2, 60% 40%)',
						gridTemplateRows: 'repeat(1, 100Vh)',
						backgroundColor: 'grey',
						gap: '20px',
						marginTop: '3Vw',
					}}
				>
					<Item style={{ gridRow: '1', backgroundColor: '#ccfff5' }}>
						<h2 className={classes.heading1}>Your ballot Box !</h2>
						<Box className={classes.box}>
							{votingStarted === false ? (
								<>
									<h3 className={classes.heading}>Voting not started yet!</h3>
								</>
							) : (
								<>
									<Button
										style={{ marginBottom: '1%' }}
										variant='contained'
										color='secondary'
										onClick={() => {
											handleClick()
										}}
										className={classes.button}
									>
										Whitelisted candidate
									</Button>
									{clicked === true ? (
										<div
											style={{
												marginLeft: '2Vw',
												marginRight: '2Vw',
												marginBottom: '2Vw',
											}}
										>
											<SnackbarProvider maxSnack={1}>
												<Table
													buttonText={'Vote'}
													onButtonClick={voteForCandidate}
													whiteListedDetails={whiteListedDetails}
												/>
											</SnackbarProvider>
										</div>
									) : (
										<></>
									)}
								</>
							)}
						</Box>
					</Item>

					<Item style={{ backgroundColor: '#0f0e0e', width: '99.6%' }}>
						{' '}
						{votingStarted === true ? (
							<>
								<div style={{ width: '80%', marginLeft: '10%' }}>
									<Winner />
								</div>
								<div style={{ marginLeft: '1Vw', marginRight: '2Vw' }}>
									<VotingStatus />
								</div>{' '}
							</>
						) : (
							<div className='fo'>
								<h1 className='a'>wait till voting being starts... </h1>
							</div>
						)}
					</Item>
				</Box>
			</div>
		</>
	)
}

export default Voter
