import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) => ({
	button: {
		margin: theme.spacing(1),
		transition: 'all 0.5s ease',
		backgroundColor: 'MediumSeaGreen',
		borderRadius: '5Vw',
		size: '50Vw',
		'&:hover': {
			backgroundColor: 'Orange',
			color: 'Green',
		},
	},
	button1: {
		margin: theme.spacing(1),
		marginTop: '5%',
		transition: 'all 0.5s ease',
		backgroundColor: 'red',

		borderRadius: '5Vw',
		size: '50Vw',
		'&:hover': {
			backgroundColor: 'white',
			color: 'Green',
		},
	},
	Typo: {
		fontFamily: 'cursive',
		fontSize: '2rem',
	},
	Icon: {
		width: '4Vw',
		fontSize: '90px',
	},
}))
