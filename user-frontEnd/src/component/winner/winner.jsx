import React, { useState } from 'react'
import Modal from 'react-modal'
import Button from '@material-ui/core/Button'
import { GiTrophyCup } from 'react-icons/gi'
import { useStyles } from './style'
import { Typography } from '@material-ui/core'
import Table from '../table/Table'
import { winner } from '../../myVotingServices /winner'

Modal.setAppElement('#root')

const Winner = () => {
	const classes = useStyles()

	const [voteCount, setVoteCount] = useState({})
	const [open, setOpen] = useState(false)
	let winnerDetails = []

	const getWinner = async () => {
		setOpen(true)
		let winnerDetails = await winner()
		await setVoteCount({ name: winnerDetails[1], address: winnerDetails[0] })
	}

	let winnerData = { name: voteCount.name, address: voteCount.address }
	winnerDetails.push(winnerData)

	return (
		<div>
			<Button
				variant='contained'
				color='primary'
				className={classes.button}
				fullWidth='true'
				endIcon={<GiTrophyCup className={classes.Icon}>Winner</GiTrophyCup>}
				onClick={() => {
					getWinner()
				}}
			>
				<Typography className={classes.Typo}>Winner</Typography>
			</Button>

			<Modal
				isOpen={open}
				onRequestClose={() => setOpen(false)}
				style={{
					overlay: {
						backgroundColor: 'pink',
						margin: '10Vw',
					},
					content: {
						backgroundColor: 'grey',
					},
				}}
			>
				<Typography
					style={{
						fontSize: '40Px',
						fontFamily: 'sans-serif',
						marginLeft: '45%',
						textDecorationColor: 'white',
						textDecorationLine: 'initial',
					}}
				>
					{' '}
					Winner
				</Typography>
				<div style={{ marginLeft: '18Vw' }}>
					{open === true ? (
						<Table
							buttonText={'Winner'}
							onButtonClick={''}
							whiteListedDetails={winnerDetails}
						/>
					) : (
						<> </>
					)}
				</div>
				<>
					<Button
						variant='contained'
						color='primary'
						className={classes.button1}
						fullWidth='true'
						endIcon={<GiTrophyCup className={classes.Icon}>Winner</GiTrophyCup>}
						onClick={() => {
							setOpen(false)
						}}
					>
						{' '}
						Close
					</Button>
				</>
			</Modal>
		</div>
	)
}

export default Winner
