import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import App from './App'
import Web3 from 'web3'
import { io } from 'socket.io-client'
import axios from 'axios'
import reportWebVitals from './reportWebVitals'
import store from './redux/store/store'
import { BrowserRouter } from 'react-router-dom'
import { setVotingStatus } from '../src/redux/reducer/VotingStatus'
import { setVotingLog } from './redux/reducer/votingLog'
import { contract } from './const/contractDetail'
import { whitelistCandidateEvent } from './myVotingServices /whiteListedCandidateEvent'
import { votingStatusEvent } from './myVotingServices /votingStatusEvent'
import { isVoting } from './myVotingServices /isVoting'
import { getRemovedRegisteredCandidateAfterRemoveEvent } from './myVotingServices /removeRegisteredCandidateEvent'
import { registeredCandidateEvent } from './myVotingServices /registeredCandidateEvent'
const web3 = new Web3(window.web3.currentProvider)

window.ethereum.enable()

export const contractInstance = new web3.eth.Contract(
	contract.ABI,
	contract.address
)

const socket = io('http://localhost:8080')
socket.on('connect', () => {})

socket.on('votingLog', (data) => {
	let votingDetailLog = {
		voterAddress: data.voterAddress,
		candidateAddress: data.candidateAddress,
	}
	store.dispatch(setVotingStatus([votingDetailLog]))
})

registeredCandidateEvent()
whitelistCandidateEvent()
votingStatusEvent()
getRemovedRegisteredCandidateAfterRemoveEvent()

const init = async () => {
	const result = await axios.get('/previousVotingLog')
	const votingStatus = await isVoting()
	let votingDetails = []

	result !== undefined
		? result.data.forEach((log, index) => {
				let votingDetailLog = {
					voterAddress: log.voter_Address,
					candidateAddress: log.candidate_Address,
				}
				console.log(votingDetailLog)
				votingDetails.push(votingDetailLog)
		  })
		: console.log(result)

	store.dispatch(setVotingStatus(votingDetails))
	store.dispatch(setVotingLog(votingStatus))
}

init()

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<Provider store={store}>
				<App />
			</Provider>
		</BrowserRouter>
	</React.StrictMode>,
	document.getElementById('root')
)

reportWebVitals()
