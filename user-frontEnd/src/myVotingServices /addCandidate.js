import { contractInstance } from '../index'

export const addCandidate = async (name) => {
	try {
		const accounts = await window.ethereum.request({
			method: 'eth_requestAccounts',
		})
		const account = accounts[0]
		console.log('selectedAcconds', account)
		await contractInstance.methods
			.addCandidate(name)
			.send({ from: window.ethereum.selectedAddress })
	} catch (e) {
		console.log('Error', e)
	}
}
