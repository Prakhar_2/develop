import { contractInstance } from '../index'

export const getAdmin = async () => {
	try {
		const result = await contractInstance.methods.owner.call().call()
		return result
	} catch (e) {
		console.log('error in getting admin', e)
	}
}
