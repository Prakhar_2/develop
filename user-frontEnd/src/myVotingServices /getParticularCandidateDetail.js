import { contractInstance } from '../index'

export const getParticularCandidateDetail = async (candidateAddress) => {
	try {
		const res = await contractInstance.methods
			.getCandidateDetail(candidateAddress)
			.call()
		return res
	} catch (e) {
		console.log('Error in getting particular candidate', e)
	}
}
