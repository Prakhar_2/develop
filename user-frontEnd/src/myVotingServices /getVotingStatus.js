import { contractInstance } from '../index'

export const getVotingStatus = async () => {
	try {
		const res = await contractInstance.methods.getVotingStatus().call()
		return res
	} catch (e) {
		console.log('ERROR IS', e)
	}
}
