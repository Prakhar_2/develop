import { contractInstance } from '../index'

export const getWhiteListed = async () => {
	try {
		let res = await contractInstance.methods.getWhiteListed().call()
		return res
	} catch (e) {
		console.log('Error getting', e.message.error)
	}
}
