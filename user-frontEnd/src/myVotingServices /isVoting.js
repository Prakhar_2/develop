import { contractInstance } from '../index'

export const isVoting = async () => {
	const result = await contractInstance.methods.isVoting.call().call()
	return result
}
