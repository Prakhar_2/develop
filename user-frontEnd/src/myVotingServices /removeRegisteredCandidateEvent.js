import { contractInstance } from '../index'
import store from '../redux/store/store'
import { setRemovedRegisteredCandidate } from '../redux/reducer/removeRegistredCandidate'

export const getRemovedRegisteredCandidateAfterRemoveEvent = async () => {
	try {
		await contractInstance.events.getRegisteredCandidateAfterRemoveEvent(
			{},
			function (error, event) {
				console.log('event is removed ', event)
				if (event !== undefined) {
					console.log('event in removed is ', event)
					store.dispatch(
						setRemovedRegisteredCandidate([event.returnValues.candidateAddress])
					)
				}
			}
		)
	} catch (e) {
		console.log('error in event ', e)
	}
}
