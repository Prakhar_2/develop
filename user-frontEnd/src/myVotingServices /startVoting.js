import { contractInstance } from '../index'
import { contract } from '../const/contractDetail'

export const startVoting = async () => {
	const selectedAddress = window.ethereum.selectedAddress

	try {
		if (selectedAddress === contract.ownerAddress) {
			await contractInstance.methods
				.startVoting()
				.send({ from: contract.ownerAddress })
		} else {
			alert('Plzz select right address')
		}
	} catch (e) {
		console.log('Error', e)
	}
}
