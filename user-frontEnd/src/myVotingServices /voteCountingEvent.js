import { contractInstance } from '../index'
import store from '../redux/store/store'
import { setVotingStatus } from '../redux/reducer/VotingStatus'

export const VoteCountingEvent = async () => {
	try {
		await contractInstance.events.VoteCountingEvent(
			{},
			function (error, event) {
				if (event !== undefined) {
					store.dispatch(
						setVotingStatus([
							{
								VoterName: event.returnValues.voterAddress,
								candidateName: event.returnValues.candidateAddress,
							},
						])
					)
					console.log(
						'voting  status is',
						event.returnValues.voterAddress,
						event.returnValues.candidateAddress
					)
				}
			}
		)
	} catch (e) {
		console.log('error in event ', e)
	}
}
