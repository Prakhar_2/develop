import { contractInstance } from '../index'
import store from '../redux/store/store'
import { setVotingLog } from '../redux/reducer/votingLog'

export const votingStatusEvent = async () => {
	try {
		await contractInstance.events.votingStatusEvent(
			{},
			function (error, event) {
				if (event !== undefined) {
					store.dispatch(setVotingLog(event.returnValues.votingStatus))
				}
			}
		)
	} catch (e) {
		console.log('error in event ', e)
	}
}
