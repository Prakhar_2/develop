import { contractInstance } from '../index'

export const winner = async () => {
	try {
		const res = await contractInstance.methods.winner().call()
		return res
	} catch (e) {
		alert('Error', e)
	}
}
