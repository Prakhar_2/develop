import { createSlice } from '@reduxjs/toolkit'

export const votingStatus = createSlice({
	name: 'votingStatus',
	initialState: {
		value: [],
	},
	reducers: {
		setVotingStatus: (state, action) => {
			state.value.push(action.payload)
		},
	},
})

export const { setVotingStatus } = votingStatus.actions
export default votingStatus.reducer
