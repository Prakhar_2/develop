import { createSlice } from '@reduxjs/toolkit'

export const whiteListedCandidate = createSlice({
	name: 'witeListedCandidate',
	initialState: {
		whiteListedCandidates: [],
	},
	reducers: {
		setWhiteListedCandidate: (state, action) => {
			state.whiteListedCandidates.push(action.payload)
		},
	},
})

export const { setWhiteListedCandidate } = whiteListedCandidate.actions
export default whiteListedCandidate.reducer
