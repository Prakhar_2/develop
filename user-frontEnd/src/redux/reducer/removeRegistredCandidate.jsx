import { createSlice } from '@reduxjs/toolkit'

export const removeRegisteredCandidate = createSlice({
	name: 'removeRegisteredCandidate',
	initialState: {
		value: [],
		isLoading: true,
		isError: false,
	},
	reducers: {
		setRemovedRegisteredCandidate: (state, action) => {
			state.value.push(action.payload)
		},
	},
})

export const { setRemovedRegisteredCandidate } =
	removeRegisteredCandidate.actions
export default removeRegisteredCandidate.reducer
