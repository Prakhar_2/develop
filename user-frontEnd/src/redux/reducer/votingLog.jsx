import { createSlice } from '@reduxjs/toolkit'

export const votingLog = createSlice({
	name: 'votingLog',
	initialState: {
		value: false,
	},
	reducers: {
		setVotingLog: (state, action) => {
			state.value = action.payload
		},
	},
})

export const { setVotingLog } = votingLog.actions
export default votingLog.reducer
