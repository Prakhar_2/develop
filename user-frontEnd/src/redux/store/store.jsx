import {
	configureStore,
	combineReducers,
	getDefaultMiddleware,
} from '@reduxjs/toolkit'
import registeredCandidateReducer from '../reducer/RegistredCandidate'
import whiteListedCandidateReducer from '../reducer/WhiteListedCandidate'
import votingStatusReducer from '../reducer/VotingStatus'
import votingLogReducer from '../reducer/votingLog'
import removeRegisteredCandidateReducer from '../reducer/removeRegistredCandidate'

const reducer = combineReducers({
	registeredCandidate: registeredCandidateReducer,
	whiteListedCandidate: whiteListedCandidateReducer,
	votingStatus: votingStatusReducer,
	votingLog: votingLogReducer,
	removedRegisteredCandidate: removeRegisteredCandidateReducer,
})

const store = configureStore({
	reducer,
	middleware: [...getDefaultMiddleware({ thunk: false })],
})

export default store
