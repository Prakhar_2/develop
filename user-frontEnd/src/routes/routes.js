import React, { useState, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { useSelector } from 'react-redux'
import CandidateList from '../component/user/candidateList/candidateList'
import { showWhiteListed } from '../myVotingServices /showWhiteListed'
import Voter from '../component/user/voter/voter'
import { Typography } from '@material-ui/core'

const Routes = () => {
	const votingStatus = useSelector((state) => state.votingLog.value)
	const [isCandidate, setCandidateStatus] = useState(false)
	let selectedAddressIs = ''

	if (window.ethereum.selectedAddress !== null) {
		selectedAddressIs = window.ethereum.selectedAddress.toLowerCase()
	}

	const [selectedAddress, setSelectedAddress] = useState([selectedAddressIs])

	window.ethereum.on('accountsChanged', (accounts) => {
		setSelectedAddress(accounts[accounts.length - 1])
	})

	useEffect(() => {
		const init = async () => {
			let flag = 0
			let whiteListedCandidates = await showWhiteListed()

			for (let candidate of whiteListedCandidates) {
				if (
					candidate.toLowerCase() === selectedAddress ||
					candidate.toLowerCase() === selectedAddress[0]
				) {
					flag = 1
					break
				}
			}
			if (flag === 1) {
				setCandidateStatus(true)
			} else setCandidateStatus(false)
		}
		init()
	})

	return (
		<div>
			<Switch>
				<Route exact path='/'>
					<div
						style={{ margin: '3Vh', height: '100Vh', backgroundColor: 'azure' }}
					>
						<Voter />
					</div>
				</Route>

				<Route exact path='/voter'>
					<div
						style={{ margin: '3Vh', height: '100Vh', backgroundColor: 'azure' }}
					>
						<Voter />
					</div>
				</Route>

				<Route exact path='/Candidate'>
					{(votingStatus === true && isCandidate === true) ||
					votingStatus === false ? (
						<div
							style={{
								margin: '3Vh',
								height: '100Vh',
								backgroundColor: 'azure',
							}}
						>
							<CandidateList />
						</div>
					) : (
						<div
							style={{
								height: '100Vh',
								width: '100%',
								position: 'fixed',
								backgroundColor: 'azure',
							}}
						>
							<Typography
								style={{
									margin: '50px',
									marginTop: '20rem',
									fontFamily: 'serif',
									fontSize: '40px',
								}}
							>
								YOU ARE NOT A CANDIDATE !
							</Typography>{' '}
						</div>
					)}
				</Route>
			</Switch>
		</div>
	)
}

export default Routes
